// TREASURE HUNT HUD
//
// This is the script that must be in a prim worn as a HUD.
// It gives the current info about collected treasures by the user.

// Give your own channel here. Something long.
integer chan = 1643765;

// This is the object the user will receive after collecting all the treasures. 
// Put the UUID of the price.
key price = "16f65e36-fc2d-afa1-9baf-f40ea8fc516f";

// This is the total number of treasures to find.
integer totaltreasure = 5;

// The messages that will appear as a title in the hud. Followed by
// "secrets found: " to show how many secrets the user has found.
string texthud = "Secret Hunt Hud\nSecrets found: ";

// The IM the HUD will send when a trasure is found.
string found = "You found a treasure!";

// The IM the HUD will send if a treasure has been already found.
string already = "You already found this secret!";

// The text that will appear in a dialog box as a help for the user.
string helptext = "This is a secret hunt. Secrets are all over the sim. Find them and click them all to get a special price!";

// The welcome message appearing when the HUD is started for the first time
string welcome = "Secret Hunt Hud has started! Find the secrets to get a secret present from the sim!!";

// The message appearing when all the treasures got found
string userwon = "You found all the treasures of the island! Prepare to receive your reward! Once you get it, you can detach this HUD.";

// Add as many treasures as totaltreasure says.
// I tried to make this automatic but LSL can't do this, apparently
integer tr1 = FALSE;
integer tr2 = FALSE;
integer tr3 = FALSE;
integer tr4 = FALSE;
integer tr5 = FALSE;

// Some other junk
integer treasurefound = 0;
key user;

findingtreasure(integer treasureid)  //this function is triggered when a treasure has been found
{
	if treasureid = FALSE;
	{
		llOwnerSay(found);
		++treasurefound;
		treasureid = TRUE;
		llSetText(texthud + (string)treasurefound + "/" + (string)totaltreasure, <1,1,1>, 1.5);
		if (treasurefound == totaltreasure)
		{
			llOwnerSay(userwon);
			llGiveInventory(user, price);
			llSetText(texthud + "ALL", <1,0,0>, 1.5);
		}
	}

	else
	{
		llOwnerSay(already);
	}
}

default
{
	state_entry() 					// Booting the script
	{
		user = llGetOwner();
		llListen(chan,"","","");	// Sets a listener to the chan only to reduce lag as much as possible
		llSetText(texthud + (string)treasurefound + "/" + (string)totaltreasure, <1,1,1>, 1.5);  // Sets text
		llOwnerSay(welcome); // Welcome message.
	}
    
	touch_start(integer total_number)
	{
		llDialog(user,helptext,["OK"], 1);		// When the hud is clicked, sends a dialog with a help text
	}
	
	listen(integer chan, string name, key id, string m)
	{	
		// You must add and increment this manually
		if (m == "treasure1" + (string)user) 	// When a treasure got clicked (receiving the secret code)
		{
			findingtreasure(tr1);
		}
		
		if (m == "treasure2" + (string)user) 	// When a treasure got clicked (receiving the secret code)
		{
			findingtreasure(tr2);
		}
		
		if (m == "treasure3" + (string)user) 	// When a treasure got clicked (receiving the secret code)
		{
			findingtreasure(tr3);
		}
		
		if (m == "treasure4" + (string)user) 				// When a treasure got clicked (receiving the secret code)
		{
			findingtreasure(tr4);
		}
		
		if (m == "treasure5" + (string)user) 				// When a treasure got clicked (receiving the secret code)
        	{
			findingtreasure(tr5);
		}
       	}
}
