# SL_Treasure_Hunt
A simple secret hunt set of scripts for Second Life.

### How does it work?
This is a set of scripts made for sim owners who wants to put some secret hunt event in their sim. They put small little things hidden in the sim. The game for the user is to wear the secret treasure hud and click on these secrets. When the user found them all, they win a prize. 

### Instructions
First, you edit the hud script for your needs. Changing the texts in the strings, changing the number of treasures and so on. Instructions are in the comments of the script.

Then you build the hud. It can be a single prim with your logo as a texture. You put the script on it, then you start the script.

Now, to set the treasures, you need to edit the treasure script following your needs. Instructions are in comments on the script. Then, you put the script on the secret and you start it.

Once all your secrets are installed, you're ready! You now just need to distribute the HUD to your customers. :p (don't forget to set the hud script at least as no modify before release or everybody will see the prize uuid!)
