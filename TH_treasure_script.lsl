// TREASURE HUNT TREASURE SCRIPT
//
// This is the script that must be in all the treasures you want your users to find.

// This must be the same chan defined in the hud script
integer chan = 1643765;

// Put here the id of the treasure. Between 1 to the number you set in totaltreasure
integer id = 1;

// Put here a description of the treasure itself.
string text = "This is a secret to everybody.";

default
{
    touch_start(integer total_number)
    {
        llSay(0, text);
        llSay(chan, "treasure" + (string)id + (string)llDetectedKey(0));
    }
}
